FROM java:8-jdk-alpine
LABEL version="1.0.0" description="Projeto  desafio iByte - Symon" maintainer="Symon Lopes<symoncc@gmail.com>"

COPY ./projeto-desafio-symon-0.0.1-SNAPSHOT.war /usr/app/
WORKDIR /usr/app
	
EXPOSE 8080



ENTRYPOINT ["java", "-jar", "projeto-desafio-symon-0.0.1-SNAPSHOT.war"]