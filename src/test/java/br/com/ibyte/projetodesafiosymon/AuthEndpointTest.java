package br.com.ibyte.projetodesafiosymon;

import java.net.URL;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
class AuthEndpointTest {

	/*
	 * Testes que faltam escrever: Validar quando o token JWT estiver inválido.
	 */

	@LocalServerPort
	private int port;

	private URL base;

	@BeforeEach
	public void setUp() throws Exception {
		this.base = new URL("http://localhost:" + port + "/api");
	}

	@Test
	public void tentarObterTokenSemCredenciais() throws Exception {

		String retorno = "";
		HttpPost post = new HttpPost(this.base.toString().concat("/auth"));

		post.setEntity(new StringEntity("{ \"usuario\":\"springuser\", \"senha\":\"password\" }"));
		post.addHeader("content-type", "application/json");

		try (CloseableHttpClient httpClient = HttpClients.createDefault();
				CloseableHttpResponse response = httpClient.execute(post)) {
			System.out.println(response);
			retorno = EntityUtils.toString(response.getEntity());
		}
		System.out.println("retorno");
	}

}
