package br.com.ibyte.projetodesafiosymon;

import java.net.URL;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class SemAuteitencacaoTestes {

	@LocalServerPort
	private int port;

	private URL base;

	@Autowired
	private TestRestTemplate template;

	@BeforeEach
	public void setUp() throws Exception {
		this.base = new URL("http://localhost:" + port + "/api/setor");
	}

	@Test
	public void tentarListarSetoresSemAutenticacao() throws Exception {
//		Setor setor = new Setor();
//		HttpEntity<Setor> request = new HttpEntity<Setor>(setor);
//		ResponseEntity<String> result = this.template.postForEntity(base.toString().concat("/cadastrar"), request,
//				String.class);
//		Assertions.assertThat(result.getStatusCodeValue()).isEqualTo(401);

		Assertions.assertThrows(NumberFormatException.class, () -> {
			Integer.parseInt("One");
		});
	}

}
