package br.com.ibyte.projetodesafiosymon;

import java.net.URL;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import br.com.ibyte.projetodesafiosymon.model.Pessoa;
import br.com.ibyte.projetodesafiosymon.utils.TokenUtils;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
class PessoaEndpointTest {

	@LocalServerPort
	private int port;

	private URL base;

	@Autowired
	private TestRestTemplate template;

	private String token;

	@BeforeEach
	public void setUp() throws Exception {
		this.base = new URL("http://localhost:" + port + "/api");
		token = TokenUtils.obterTokenJWT(base.toString().concat("/auth"));
	}

	@Test
	public void tentarCadastrarPessoaSemDados() throws Exception {
		HttpHeaders headers = TokenUtils.criarHeaderAutenticacao(this.token);
		Pessoa pessoa = new Pessoa();
		HttpEntity<Pessoa> request = new HttpEntity<Pessoa>(pessoa, headers);
		ResponseEntity<String> result = this.template.postForEntity(base.toString().concat("/pessoa/salvar"), request,
				String.class);
		Assertions.assertEquals(400, result.getStatusCodeValue());
	}

	@Test
	@Order(1)
	public void cadastrarNovaPessoa() throws Exception {
		Pessoa pessoa = new Pessoa();
		pessoa.setPrimeiroNome("Jhon");
		pessoa.setSegundoNome("Doe");
		pessoa = salvarPessoa(pessoa);
		Assertions.assertEquals(1, pessoa.getId());

	}

	@Test
	@Order(2)
	public void listar() throws Exception {
		String uri = base.toString().concat("/pessoa/listar");
		Response res = RestAssured.given().log().all()
				.header("Authorization", this.token, "Content-Type", ContentType.JSON).when().get(uri).then()
				.contentType(ContentType.JSON).extract().response();

		String primeiroNome = res.jsonPath().getString("primeiroNome[0]");
		String segundoNome = res.jsonPath().getString("segundoNome[0]");

		Assertions.assertEquals("Jhon", primeiroNome);
		Assertions.assertEquals("Doe", segundoNome);
	}

	@Test
	@Order(3)
	public void atualizar() throws Exception {
		Pessoa pessoa = obterPeloId(1L);
		pessoa.setPrimeiroNome("Primeiro Nome Editado");
		pessoa = salvarPessoa(pessoa);
		Assertions.assertEquals("Primeiro Nome Editado", pessoa.getPrimeiroNome());
	}

	@Test
	@Order(4)
	public void apagar() throws Exception {
		Pessoa b = obterPeloId(1L);
		int status = apagar(b.getId());
		Assertions.assertEquals(200, status);
		b = obterPeloId(1L);
		Assertions.assertEquals(null, b);
	}

	@Test
	@Order(5)
	public void apagarIdInexistente() throws Exception {
		int status = apagar(15115L);
		Assertions.assertEquals(404, status);
	}

	private int apagar(Long id) {
		Response response = RestAssured.given().header("Authorization", this.token, "Content-Type", ContentType.JSON)
				.when().delete(base.toString().concat("/pessoa/").concat(id.toString()));

		int status = response.getStatusCode();
		return status;
	}

	private Pessoa salvarPessoa(Pessoa pessoa) {
		HttpHeaders headers = TokenUtils.criarHeaderAutenticacao(this.token);
		HttpEntity<Pessoa> request = new HttpEntity<Pessoa>(pessoa, headers);
		ResponseEntity<Pessoa> result = this.template.postForEntity(base.toString().concat("/pessoa/salvar"), request,
				Pessoa.class);
		if (result.getStatusCodeValue() == 200) {
			return result.getBody();
		} else {
			return null;
		}
	}

	private Pessoa obterPeloId(Long id) {
		String uri = base.toString().concat("/pessoa/").concat(id.toString());
		Response response = RestAssured.given().header("Authorization", this.token, "Content-Type", ContentType.JSON)
				.when().get(uri);

		if (response.getStatusCode() == 200) {
			return response.as(Pessoa.class);
		} else {
			return null;
		}
	}

}
