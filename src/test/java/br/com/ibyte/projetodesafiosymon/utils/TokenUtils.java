package br.com.ibyte.projetodesafiosymon.utils;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.http.HttpHeaders;

public class TokenUtils {

	public static String obterTokenJWT(String endpoint) throws Exception {
		String retorno = "";
		HttpPost post = new HttpPost(endpoint);

		post.setEntity(new StringEntity("{ \"usuario\":\"springuser\", \"senha\":\"password\" }"));
		post.addHeader("content-type", "application/json");

		try (CloseableHttpClient httpClient = HttpClients.createDefault();
				CloseableHttpResponse response = httpClient.execute(post)) {

			retorno = EntityUtils.toString(response.getEntity());
		}

		return retorno;

	}

	public static HttpHeaders criarHeaderAutenticacao(String token) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", token);
		return headers;
	}

}
