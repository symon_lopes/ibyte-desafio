package br.com.ibyte.projetodesafiosymon;

import java.net.URL;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import br.com.ibyte.projetodesafiosymon.model.Setor;
import br.com.ibyte.projetodesafiosymon.utils.TokenUtils;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
class SetorEndpointTest {

	@LocalServerPort
	private int port;

	private URL base;

	@Autowired
	private TestRestTemplate template;

	private String token;

	@BeforeEach
	public void setUp() throws Exception {
		this.base = new URL("http://localhost:" + port + "/api");
		token = TokenUtils.obterTokenJWT(base.toString().concat("/auth"));
	}

	@Test
	public void tentaCadastrarUmSetorVazio() throws Exception {
		HttpHeaders headers = TokenUtils.criarHeaderAutenticacao(this.token);
		Setor setor = new Setor();
		HttpEntity<Setor> request = new HttpEntity<Setor>(setor, headers);
		ResponseEntity<String> result = this.template.postForEntity(base.toString().concat("/setor/salvar"), request,
				String.class);
		Assertions.assertEquals(400, result.getStatusCodeValue());
	}

	@Test
	@Order(1)
	public void insert() throws Exception {
		Setor setor = new Setor();
		setor.setDescricao("Exemplo de um setor");
		setor = salvar(setor);
		Assertions.assertEquals(1L, setor.getId());
	}

	@Test
	@Order(2)
	public void listarSetores() throws Exception {
		String uri = base.toString().concat("/setor/listar");
		Response response = RestAssured.given().log().all()
				.header("Authorization", this.token, "Content-Type", ContentType.JSON).when().get(uri).then()
				.contentType(ContentType.JSON).extract().response();
		List<Object> lista = response.jsonPath().getList("$");
		Assertions.assertEquals(1, lista.size());
	}

	@Test
	@Order(3)
	public void atualizar() throws Exception {
		Setor b = obterPeloId(1L);
		b.setDescricao("Descrição EDITADA");
		b = salvar(b);
		Assertions.assertEquals("Descrição EDITADA", b.getDescricao());
	}

	@Test
	@Order(4)
	public void apagar() throws Exception {
		Setor b = obterPeloId(1L);
		int status = apagar(b.getId());
		Assertions.assertEquals(200, status);
		b = obterPeloId(1L);
		Assertions.assertEquals(null, b);
	}

	@Test
	@Order(5)
	public void apagarIdInexistente() throws Exception {
		int status = apagar(15115L);
		Assertions.assertEquals(404, status);
	}

	private int apagar(Long id) {
		Response response = RestAssured.given().header("Authorization", this.token, "Content-Type", ContentType.JSON)
				.when().delete(base.toString().concat("/setor/").concat(id.toString()));

		int status = response.getStatusCode();
		return status;
	}

	private Setor salvar(Setor setor) {
		HttpHeaders headers = TokenUtils.criarHeaderAutenticacao(this.token);
		HttpEntity<Setor> request = new HttpEntity<Setor>(setor, headers);
		ResponseEntity<Setor> result = this.template.postForEntity(base.toString().concat("/setor/salvar"), request,
				Setor.class);
		return result.getBody();
	}

	private Setor obterPeloId(Long id) {
		String uri = base.toString().concat("/setor/").concat(id.toString());
		Response response = RestAssured.given().header("Authorization", this.token, "Content-Type", ContentType.JSON)
				.when().get(uri);

		if (response.getStatusCode() == 200) {
			return response.as(Setor.class);
		} else {
			return null;
		}
	}
}
