package br.com.ibyte.projetodesafiosymon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoDesafioSymonApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoDesafioSymonApplication.class, args);
	}

}
