package br.com.ibyte.projetodesafiosymon.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ibyte.projetodesafiosymon.exception.ResourceNotFoundException;
import br.com.ibyte.projetodesafiosymon.model.Pessoa;
import br.com.ibyte.projetodesafiosymon.persistencia.PessoaRepositorio;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

@RestController
@RequestMapping("/pessoa")
public class PessoaController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private PessoaRepositorio pessoaRepositorio;

	@ApiOperation(value = "Lista todas as pessoas cadastradas.", authorizations = {
			@Authorization(value = "jwtToken") })
	@RequestMapping(path = "/listar", method = RequestMethod.GET)
	public List<Pessoa> listar() {
		logger.debug("Listando pessoas...");
		return pessoaRepositorio.findAll();
	}

	@ApiOperation(value = "Recupera uma pessoa pelo ID.", authorizations = { @Authorization(value = "jwtToken") })
	@GetMapping("/{id}")
	public Pessoa obterPeloId(@PathVariable(value = "id") Long id) {
		logger.debug("Obtendo pessoa pelo id...");
		return pessoaRepositorio.findById(id).orElseThrow(() -> new ResourceNotFoundException());
	}

	@ApiOperation(value = "Insere ou atualiza os dadode uma pessoa.", authorizations = {
			@Authorization(value = "jwtToken") })
	@PostMapping("/salvar")
	public Pessoa cadastrar(@Valid @RequestBody Pessoa pessoa) {
		logger.debug("Cadastrando nova pessoa...");
		Pessoa updated = pessoaRepositorio.save(pessoa);
		return updated;
	}

	@ApiOperation(value = "Apaga uma pessoa do banco de dados usando o ID.", authorizations = {
			@Authorization(value = "jwtToken") })
	@DeleteMapping("/{id}")
	public void apagar(@PathVariable(value = "id") Long id) {
		logger.debug("Apagando entidade pessoa...");
		pessoaRepositorio.findById(id).orElseThrow(() -> new ResourceNotFoundException());
		pessoaRepositorio.deleteById(id);
	}

}
