package br.com.ibyte.projetodesafiosymon.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ibyte.projetodesafiosymon.business.JWTBusiness;
import br.com.ibyte.projetodesafiosymon.jwt.JwtUserDetailsService;
import br.com.ibyte.projetodesafiosymon.vo.DadosAutenticacaoUsuarioVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class AutenticacaoController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JWTBusiness jwtBusiness;

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@ApiOperation(value = "Valida credenciais usuário e, em caso de sucesso retorna um token JWT para ser usado nas demais requests.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Autenticação ocorrida com sucesso."),
			@ApiResponse(code = 400, message = "Erro de validação nos dados fornecidos.") })
	@RequestMapping(value = "/auth", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<?> createAuthenticationToken(@Valid @RequestBody DadosAutenticacaoUsuarioVO dadosUsuario)
			throws Exception {
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(dadosUsuario.getUsuario(), dadosUsuario.getSenha()));
			final UserDetails userDetails = userDetailsService.loadUserByUsername(dadosUsuario.getUsuario());
			final String token = jwtBusiness.generateToken(userDetails);
			return ResponseEntity.ok("Bearer ".concat(token));
		} catch (DisabledException e) {
			return ResponseEntity.badRequest().build();
		}
	}

}
