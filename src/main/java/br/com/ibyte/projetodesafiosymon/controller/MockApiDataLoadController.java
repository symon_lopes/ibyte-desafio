package br.com.ibyte.projetodesafiosymon.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import br.com.ibyte.projetodesafiosymon.model.Pessoa;
import br.com.ibyte.projetodesafiosymon.model.Setor;
import br.com.ibyte.projetodesafiosymon.persistencia.PessoaRepositorio;
import br.com.ibyte.projetodesafiosymon.persistencia.SetorRepositorio;
import br.com.ibyte.projetodesafiosymon.vo.MockApiEmployerVO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

@RestController
@RequestMapping("/data")
public class MockApiDataLoadController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SetorRepositorio setorRepositorio;

	@Autowired
	private PessoaRepositorio pessoaRepositorio;

	@ApiOperation(value = "Importa arquivo  dados da API https://5e61af346f5c7900149bc5b3.mockapi.io/desafio03/employer", authorizations = {
			@Authorization(value = "jwtToken") })
	@RequestMapping(path = "/importar", method = RequestMethod.GET)
	public void importar() {

		try {
			String data = new RestTemplate()
					.getForObject("http://5e61af346f5c7900149bc5b3.mockapi.io/desafio03/employer", String.class);
			ArrayList<MockApiEmployerVO> empregados = new Gson().fromJson(data,
					new TypeToken<List<MockApiEmployerVO>>() {
					}.getType());

			for (MockApiEmployerVO empl : empregados) {
				try {
					Setor setor = new Setor();
					setor.setDescricao(empl.getDepartament());
					setorRepositorio.save(setor);
				} catch (DataIntegrityViolationException e) {
					logger.debug("Já existe um setor com essa descrição, ignorando.");
				}

			}

			for (MockApiEmployerVO empl : empregados) {
				try {
					Pessoa pessoa = new Pessoa();
					pessoa.setPrimeiroNome(empl.getFirst_name());
					pessoa.setSegundoNome(empl.getLast_name());
					Optional<Setor> opt = setorRepositorio.buscarPelaDescricao(empl.getDepartament());
					Setor setorDoFuncionario = opt.get();
					pessoa.setSetor(setorDoFuncionario);

					pessoaRepositorio.save(pessoa);
				} catch (DataIntegrityViolationException e) {
					logger.debug("Erro ao importar empregado.");
				}

			}

		} catch (Exception e) {
			logger.error("IOException", e);
		}
	}

}
