package br.com.ibyte.projetodesafiosymon.controller;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ibyte.projetodesafiosymon.exception.ResourceNotFoundException;
import br.com.ibyte.projetodesafiosymon.model.Pessoa;
import br.com.ibyte.projetodesafiosymon.model.Setor;
import br.com.ibyte.projetodesafiosymon.persistencia.SetorRepositorio;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

@RestController
@RequestMapping("/setor")
public class SetorController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SetorRepositorio setorRepositorio;

	@ApiOperation(value = "Lista todos os setores cadastrados.", authorizations = {
			@Authorization(value = "jwtToken") })
	@RequestMapping(path = "/listar", method = RequestMethod.GET)
	public List<Setor> listar() {
		logger.debug("Listando setores...");
		return setorRepositorio.findAll();
	}

	@ApiOperation(value = "Recupera um setor pelo ID.", authorizations = { @Authorization(value = "jwtToken") })
	@GetMapping("/{id}")
	public Setor obterPeloId(@PathVariable(value = "id") Long id) {
		logger.debug("Obtendo setor pelo id...");
		Setor setor = setorRepositorio.findById(id).orElseThrow(() -> new ResourceNotFoundException());
		return setor;
	}

	@ApiOperation(value = "Lista as pessoas de um determinado setor usando a descrição do setor.", authorizations = {
			@Authorization(value = "jwtToken") })
	@GetMapping("/descricao/{descricaoSetor}/pessoas")
	public Set<Pessoa> obterPessoasPorSetor(@PathVariable(value = "descricaoSetor") String descricaoSetor) {
		logger.debug("Obtendo pessoas por descricao setor setor...");
		Setor setor = setorRepositorio.buscarPelaDescricao(descricaoSetor)
				.orElseThrow(() -> new ResourceNotFoundException());
		return setor.getPessoas();
	}

	@ApiOperation(value = "Lista as pessoas de um determinado setor.", authorizations = {
			@Authorization(value = "jwtToken") })
	@GetMapping("/{id}/pessoas")
	public Set<Pessoa> obterPessoasDescricaoSetor(@PathVariable(value = "id") Long id) {
		logger.debug("Obtendo pessoas por setor...");
		Setor setor = setorRepositorio.findById(id).orElseThrow(() -> new ResourceNotFoundException());
		return setor.getPessoas();
	}

	@ApiOperation(value = "Insere ou atualiza um registro de Setor.", authorizations = {
			@Authorization(value = "jwtToken") })
	@PostMapping("/salvar")
	public Setor cadastrar(@Valid @RequestBody Setor setor) {
		logger.debug("Cadastrando novo setor...");
		return setorRepositorio.save(setor);
	}

	@ApiOperation(value = "Apaga um determinado setor usando o ID.", authorizations = {
			@Authorization(value = "jwtToken") })
	@DeleteMapping("/{id}")
	public void apagar(@PathVariable(value = "id") Long id) {
		logger.debug("Apagando entidade setor...");
		setorRepositorio.findById(id).orElseThrow(() -> new ResourceNotFoundException());
		setorRepositorio.deleteById(id);
	}

}
