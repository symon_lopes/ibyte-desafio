package br.com.ibyte.projetodesafiosymon.vo;

import java.io.Serializable;

public class MockApiEmployerVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String first_name;
	private String last_name;
	private String career;
	private String departament;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getCareer() {
		return career;
	}

	public void setCareer(String career) {
		this.career = career;
	}

	public String getDepartament() {
		return departament;
	}

	public void setDepartament(String departament) {
		this.departament = departament;
	}

}
