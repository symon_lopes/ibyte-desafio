package br.com.ibyte.projetodesafiosymon.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

public class JwtAutenticacaoResposta implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Token JWT.")
	private String tokenjwt;

	public JwtAutenticacaoResposta(String token) {
		tokenjwt = token;
	}

	public String getTokenjwt() {
		return tokenjwt;
	}

	public void setTokenjwt(String tokenjwt) {
		this.tokenjwt = tokenjwt;
	}

}
