package br.com.ibyte.projetodesafiosymon.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

public class DadosAutenticacaoUsuarioVO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Nome de usuário. Default: springuser", required = true, example = "springuser")
	private String usuario;

	@ApiModelProperty(value = "Senha do usuário. Default: password", required = true, example = "password")
	private String senha;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
