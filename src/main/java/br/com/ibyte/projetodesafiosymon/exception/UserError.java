package br.com.ibyte.projetodesafiosymon.exception;

public class UserError {

	private String descricao;

	public UserError(String descricao) {
		super();
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
