package br.com.ibyte.projetodesafiosymon.exception;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class GenericExceptionHandler {

	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<?> handleException(DataIntegrityViolationException e) {
		UserError response = new UserError(
				"Erro de integridade nos dados (Verifique se não forneceu algum ID que não existe na base de dados).");
		return new ResponseEntity<UserError>(response, HttpStatus.CONFLICT);

	}

	@ExceptionHandler(value = BadCredentialsException.class)
	public ResponseEntity<?> credenciaisInvalidas(BadCredentialsException e) {
		UserError response = new UserError("Parece que suas credenciais não são válidas.");
		return new ResponseEntity<UserError>(response, HttpStatus.BAD_REQUEST);

	}

}
