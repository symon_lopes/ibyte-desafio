package br.com.ibyte.projetodesafiosymon.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;

@Entity
@Table(name = "setor")
@JsonInclude(Include.NON_NULL)
public class Setor {

	@Id
	@ApiModelProperty(accessMode = AccessMode.READ_ONLY)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "setor_sequence")
	private Long id;

	@ApiModelProperty(value = "Descrição do setor. Exemplo.: Contabilidade, Pessoal, Financeiro, etc.", example = "Descrição de Exemplo do Setor")
	@Column(name = "descricao", nullable = false, unique = true)
	@NotBlank(message = "A descrição do setor é obrigatória.")
	@Size(min = 3, max = 100, message = "A descrição do setor precisa ter no mínimo 3 caracteres e no máximo 100.")
	private String descricao;

	@ApiModelProperty(required = false, hidden = true)
	@JsonManagedReference
	@OneToMany(mappedBy = "setor", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Pessoa> pessoas;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getId() {
		return id;
	}

	public Set<Pessoa> getPessoas() {
		return pessoas;
	}

	public void setPessoas(Set<Pessoa> pessoas) {
		this.pessoas = pessoas;
	}

}
