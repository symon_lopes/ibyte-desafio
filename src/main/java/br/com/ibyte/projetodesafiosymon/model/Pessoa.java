package br.com.ibyte.projetodesafiosymon.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;

@Entity
@Table(name = "pessoa")
@JsonInclude(Include.NON_NULL)
public class Pessoa {

	@Id
	@ApiModelProperty(accessMode = AccessMode.READ_ONLY, hidden = true)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pessoa_sequence")
	private Long id;

	@ApiModelProperty(value = "Primeiro nome da pessoa.", example = "Jhon")
	@Column(name = "primeiro_nome", nullable = false)
	@NotBlank(message = "O campo 'primeiro nome' é obrigatório.")
	private String primeiroNome;

	@ApiModelProperty(value = "Segundo nome da pessoa.", example = "Doe")
	@Column(name = "segundoNome", nullable = false)
	@NotBlank(message = "O campo 'segundo nome' é obrigatório.")
	private String segundoNome;

	@ApiModelProperty(value = "Setor ao qual o usuário pertence (passe somente o ID)", required = false)
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "setor_id", nullable = true)
	@JsonBackReference
	private Setor setor;

	public Long getId() {
		return id;
	}

	public String getPrimeiroNome() {
		return primeiroNome;
	}

	public void setPrimeiroNome(String primeiroNome) {
		this.primeiroNome = primeiroNome;
	}

	public String getSegundoNome() {
		return segundoNome;
	}

	public void setSegundoNome(String segundoNome) {
		this.segundoNome = segundoNome;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Pessoa))
			return false;
		return id != null && id.equals(((Pessoa) o).getId());
	}

	@Override
	public int hashCode() {
		return 31;
	}
}
