package br.com.ibyte.projetodesafiosymon.persistencia;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.ibyte.projetodesafiosymon.model.Setor;

@Repository
public interface SetorRepositorio extends JpaRepository<Setor, Long> {

	@Query("SELECT s FROM Setor s where s.descricao = ?1")
	public Optional<Setor> buscarPelaDescricao(String descricao);

}
